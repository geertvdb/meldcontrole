package com.example.nzxt.meldcontrole.model;

/**
 * Created by NZXT on 3-5-2018.
 */

public class Coordinate {

    private String uid;  //primary key and key
    private long timeStamp;
    private double lat;
    private double lng;
    private String typeControle;

    public Coordinate() {
    }

    public Coordinate(String uid, long timeStamp, double lat, double lng, String typeControle) {
        this.uid = uid;
        this.timeStamp = timeStamp;
        this.lat = lat;
        this.lng = lng;
        this.typeControle = typeControle;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getTypeControle() {
        return typeControle;
    }

    public void setTypeControle(String typeControle) {
        this.typeControle = typeControle;
    }
}
