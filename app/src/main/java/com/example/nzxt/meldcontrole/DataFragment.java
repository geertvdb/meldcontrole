package com.example.nzxt.meldcontrole;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nzxt.meldcontrole.model.Coordinate;
import com.example.nzxt.meldcontrole.utils.Utility;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.List;
import java.util.UUID;


/**
 * A simple {@link Fragment} subclass.
 */
public class DataFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final int REQUEST_LOCATION = 125;
    TextView txtLat, txtLng;
    Button btnOngeval, btnShowMap, btnAlcohol, btnSnelheid;
    private boolean GpsStatus;

    private static final int PERMISSION_REQUEST_CODE = 1001;
    private static final int PLAY_SERVICE_RESOLUTION_REQUEST = 1000;

    private GoogleApiClient mGoogleApiClient = null;
    private LocationRequest mLocationRequest = null;

    private Location mLastlocation;
    private LocationManager locationManager;

    private double dblLatitude;
    private double dblLongitude;

    private boolean mRequestingLocationUpdates = false;
    private String typeControle;
    private Uri uri;
    private   String type = "";

    private static final String SETTINGS_PACKAGE = "com.android.settings";
    private static final String SETTINGS_CLASS_DATA_USAGE_SETTINGS = "com.android.settings.Settings$DataUsageSummaryActivity";

    //Firebase
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private AlertDialog dialog;

    //facebook share
    CallbackManager callbackManager;
    ShareDialog shareDialog;

    public DataFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FacebookSdk.sdkInitialize(getActivity());
        View v = inflater.inflate(R.layout.fragment_data, container, false);

        btnSnelheid = (Button) v.findViewById(R.id.btnSnelheid);
        btnAlcohol = (Button) v.findViewById(R.id.btnAlcohol);
        btnOngeval = (Button) v.findViewById(R.id.btnOngeval);
        btnShowMap = (Button) v.findViewById(R.id.btnShowMap);

        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(getActivity());

        requestPermission();

        initFirebase();

        getActivity().setFinishOnTouchOutside(true);

        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(getActivity())) {
            //Toast.makeText(getActivity(), "Gps already enabled", Toast.LENGTH_SHORT).show();
            //finish();
        }

        if (!hasGPSDevice(getActivity())) {
            Toast.makeText(getActivity(), "Gps is niet ondersteund", Toast.LENGTH_SHORT).show();
        }

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(getActivity())) {
            Log.e("keshav", "Gps already enabled");

        } else {
            Log.e("keshav", "Gps already enabled");
            //Toast.makeText(getActivity(), "Gps is ingeschakeld", Toast.LENGTH_SHORT).show();
        }

        btnSnelheid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showConfirmDialog("snelheid");
            }
        });

        btnAlcohol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showConfirmDialog("alcohol");
            }
        });

        btnOngeval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showConfirmDialog("ongeval");

            }
        });

        btnShowMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MapsActivity.class);
                startActivity(intent);
            }
        });

        return v;
    }

    private void createCoordinate(String typeControle) {

        this.typeControle = typeControle;
        long time = System.currentTimeMillis();
        Coordinate coordinate = new Coordinate(UUID.randomUUID().toString(), time, dblLatitude, dblLongitude, typeControle);
        mDatabaseReference.child("coordinates").child(coordinate.getUid()).setValue(coordinate);
        Utility.adres = Utility.getAddress(getActivity(), dblLatitude, dblLongitude);
        dialog.cancel();

        mDatabaseReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Toast.makeText(getActivity(), "Toegevoegd aan map", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private boolean amIConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void CheckGpsStatus() {

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        GpsStatus = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    //FIREBASE
    private void initFirebase() {
        FirebaseApp.initializeApp(getActivity());
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference();
    }

    private void requestPermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            //Runtime request permission
            ActivityCompat.requestPermissions(getActivity(), new String[]{
                            android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION}
                    , PERMISSION_REQUEST_CODE);
        } else {
            if (checkPlayService()) {
                buildGoogleApiClient();
                createLocationRequest();

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkPlayService()) {
                        buildGoogleApiClient();
                    }
                }
                break;
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        //fix first time run app if permission doesn't grant yet so can't get anything
        mGoogleApiClient.connect();
    }

    private void displayLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mLastlocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastlocation != null) {
            double latitude = mLastlocation.getLatitude();
            double longitude = mLastlocation.getLongitude();
            dblLatitude = mLastlocation.getLatitude();
            dblLongitude = mLastlocation.getLongitude();
        } else {
            Toast.makeText(getActivity(), "Kan geen locatie bepalen, kijk of de gps van het toestel is ingeschakeld", Toast.LENGTH_LONG).show();
        }

    }

    private boolean checkPlayService() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {

                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), PLAY_SERVICE_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getActivity(), "This device is not supported", Toast.LENGTH_LONG).show();
                getActivity().finish();
            }
            return false;
        }
        return true;
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);  // 10 sec
        mLocationRequest.setFastestInterval(3000); //  3 sec
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(10);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_LOCATION);

                            //finish();
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                }
            }
        });

    }

    public void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        displayLocation();
        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("ERROR", "Connection Failed: " + connectionResult.getErrorMessage());
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastlocation = location;
        displayLocation();
    }

    private void showFacebookDialog() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Delen");
        builder.setMessage("Wenst u dit bericht te delen op facebook?");
        builder.setCancelable(true);

        builder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

                if (!Utility.adres.equals("") && Utility.adres != null) {
                    if (typeControle.equals("ongeval")) {
                        type = "ONGEVAL";
                        Utility.urlLink = Uri.parse("https://previews.123rf.com/images/yupiramos/yupiramos1501/yupiramos150100352/35125047-dise%C3%B1o-de-los-coches-de-choque-ejemplo-gr%C3%A1fico-del-vector-eps10.jpg");
                    } else if (typeControle.equals("snelheid")) {
                        type = "SNELHEID controle";
                        Utility.urlLink = Uri.parse("https://www.onswestbrabant.nl/wms/fm/userfiles/content/blok/thumb-1400-100000/snelheidscontrole-etten-leur-1.jpg");
                    } else {
                        type = "ALCOHOL controle";
                        Utility.urlLink = Uri.parse("https://www.politie.be/5428/sites/default/files/photos/alcoholcontrole_1.jpg");
                    }
                }

                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setQuote(type + "\n" + "\n" + Utility.adres)
                        .setContentUrl(Utility.urlLink)
                        .build();
                if (ShareDialog.canShow(ShareLinkContent.class)) {
                    shareDialog.show(linkContent);
                }
            }
        });

        builder.setNegativeButton("Nee", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void showConfirmDialog(final String typeControle) {

        final int sdk = android.os.Build.VERSION.SDK_INT;
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Bevestiging");
        builder.setCancelable(false);

        LayoutInflater inflater = this.getLayoutInflater();
        View confirm_view = inflater.inflate(R.layout.confirm_layout, null);
        LinearLayout layout = (LinearLayout) confirm_view.findViewById(R.id.llConfirmDialog);


        //CHANGE BACKGROUND IMAGE
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            if (typeControle.equals("ongeval")) {
                layout.setBackgroundDrawable(getResources().getDrawable(R.drawable.ziekenwagen));
                builder.setMessage("Wit u het " + typeControle + " melden?");
            } else if (typeControle.equals("snelheid")) {
                layout.setBackgroundDrawable(getResources().getDrawable(R.drawable.snelheidsaanduiding1));
                builder.setMessage("Wit u de " + typeControle + "scontrole melden?");
            } else {
                layout.setBackgroundDrawable(getResources().getDrawable(R.drawable.alcohol));
                builder.setMessage("Wit u de " + typeControle + " controle melden?");
            }

        } else {
            if (typeControle.equals("ongeval")) {
                layout.setBackgroundResource(R.drawable.ziekenwagen);
                builder.setMessage("Wit u het " + typeControle + " melden?");
            } else if (typeControle.equals("snelheid")) {
                layout.setBackgroundResource(R.drawable.snelheidsaanduiding1);
                builder.setMessage("Wit u de " + typeControle + "scontrole melden?");
            } else {
                layout.setBackgroundResource(R.drawable.alcohol);
                builder.setMessage("Wit u de " + typeControle + "controle melden?");
            }
        }

        builder.setView(confirm_view);
        builder.setIcon(R.drawable.ic_warning_black_24dp);

        final Button btnYes = (Button) confirm_view.findViewById(R.id.btnYes);
        final Button btnNo = (Button) confirm_view.findViewById(R.id.btnNo);

        dialog = builder.create();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createCoordinate(typeControle);
                showFacebookDialog();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        dialog.show();
        // size must place after
        // dialog.getWindow().setLayout(900,900);
    }


    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {

        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        super.onStop();

    }

    @Override
    public void onResume() {
        super.onResume();
        checkPlayService();
    }


}
