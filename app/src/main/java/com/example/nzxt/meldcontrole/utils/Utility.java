package com.example.nzxt.meldcontrole.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by NZXT on 9-5-2018.
 */

public class Utility {

    public static boolean isDataEnabled;
    public static boolean isGpsEnabled;

    public static  String adres = "";
    public static Uri urlLink;

    public static String getAddress(Context context, double LATITUDE, double LONGITUDE) {

        String adres = "";
        //Set Address
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null && addresses.size() > 0) {



                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String street = addresses.get(0).getThoroughfare();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                Log.d("TAG", "getAddress:  address " + address);
                Log.d("TAG", "getAddress:  city " + city);
                Log.d("TAG", "getAddress:  state " + state);
                Log.d("TAG", "getAddress:  postalCode " + postalCode);
                Log.d("TAG", "getAddress:  knownName " + knownName);
                Log.d("TAG", "getAddress:  street " + street);

               // adres = street + " " + knownName + " " + postalCode + " " +  city + " " + country;
                adres = street +  " " + postalCode + " " +  city + " " + country;

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return adres;
    }
}
